Usage
=====

.. _installation:

Installation
------------

To use Immunogen, first install it using pip:

.. code-block:: console

   (.venv) $ pip install immunogen
