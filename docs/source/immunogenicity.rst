Immunogenicity
===

.. autosummary::
   :toctree: generated

   immunogen


    Returns a data dictionary of configuration parameters.

    Returns
    -------
    dictionary
    A dictionary of configuration parameters and their associated
    values.