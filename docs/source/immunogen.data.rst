immunogen.data package
======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   immunogen.data.pickles

Module contents
---------------

.. automodule:: immunogen.data
   :members:
   :undoc-members:
   :show-inheritance:
