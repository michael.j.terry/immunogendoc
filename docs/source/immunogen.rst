immunogen package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   immunogen.data
   immunogen.eplets
   immunogen.immunogenicity
   immunogen.utils

Module contents
---------------

.. automodule:: immunogen
   :members:
   :undoc-members:
   :show-inheritance:
