.. immunogen documentation master file, created by
   sphinx-quickstart on Tue May  3 11:56:11 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to immunogen's documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   immunogen


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
